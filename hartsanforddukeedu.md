# Hart.sanford.duke.edu/ Assessment

__<https://hart.sanford.duke.edu/>__

__Screenshot:__

![Screenshot of this website](assets/screenshot.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

Thank you for requesting an assessment. All the issues here are very common and most are easy to fix. The site is not in bad shape. In fact, it is better than most websites we do assessments on. Congrats for that!  But there are still important issues to fix to meet WCAG 2.0 AA compliance. 

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).


__Note:__

This is only an assessment of the homepage. When most items on the homepage are fixed, like color contrast issues, they will naturally propagate to the subpages.

The color contrast issues are mostly repeats.  Changing _one_ line of CSS could resolve 20 contrast issues. In some cases that one line could resolve hundreds of issues on the website as a whole, since it would be propagated to the sub pages.  So please don't be alarmed at the list. ;-)

This assessment can be used as guide for accessibility issues to look for on subpages also. For example, missing `alt` tags are prevalent on subpages. We can't document every one, but the notes here might suffice to search out similar issues.

When hart.sanford.duke.edu feels like they have implemented changes, feel free to email me and I can take a look at the homepage and some  other pages to see if I can help there too.  We encourage people to make their websites as accessible as possible, but Duke's Web Accessibility Guidelines only apply to the homepage and top level landing pages of websites. 


## Some buttons do not have an accessible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

When a button doesn't have an accessible name, screen readers announce it as "button", making it unusable for users who rely on screen readers. [Learn more](https://dequeuniversity.com/rules/axe/2.2/button-name?application=lighthouse).



### This `button` or `input` has no `value` attribute or it contains no inner text to indicate its purpose

__Visual location:__

![button is not descriptive](assets/hart-sanford-duke-edu-butto.png)

#### HTML location:

```html
<button type="submit" class="search_submit mobile_hide">
  <span class="glyphicon glyphicon-search">
  </span>
</button>
```

#### Suggested solution:

Add some invisible screen reader only text:

```html
<button type="submit" class="search_submit mobile_hide">
  <span class="glyphicon glyphicon-search">
+    <span class="sr-only">Search</span>
  </span>
</button>
```

Other ways to fix this issue:

Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element has a value attribute and the value attribute is empty
<br>Element has no value attribute or the value attribute is empty
<br>Element does not have inner text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty or not visible
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,1,HEADER,0,SECTION,2,DIV,0,DIV,0,DIV,2,DIV,2,DIV,2,FORM,1,BUTTON`<br>
Selector:<br>
`button`
</details>

---









## Some form elements do not have associated labels [WCAG 3.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#labels-or-instructions)

Labels ensure that form controls are announced properly by assistive technologies, like screen readers. [Learn more](https://dequeuniversity.com/rules/axe/2.2/label?application=lighthouse).



### This element may be missing a label

__Visual location:__

![Element missing label](assets/hart-sanford-duke-edu-.png)

__HTML location:__

```html
<input type="text" name="s" id="s" class="search_input mobile_hide font-raleway" placeholder="Search">
```

#### Suggested solution:

Add a `<label>` element with a `for` attribute to associate it with the search icon.

`+` add `-` remove `~` change

```html
+ <label for="s">Search</label>
<input type="text" name="s" id="s" class="search_input mobile_hide font-raleway" placeholder="Search">
```

Note 1: In this case you could visually hide the label using 'invisible screen reader only text' `<label class="sr-only" for="s">` so it is  still announced to screen reader users.

Note 2: `placeholder` attributes alone are not a valid substitute for `<label>`s, but having one that is descriptive is beneficial. If the label uses screen reader only text, a `placeholder` is highly suggested.

Also see "Other options" below for more valid solutions.

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty or not visible
<br>Form element does not have an implicit (wrapped) &lt;label&gt;
<br>Form element does not have an explicit &lt;label&gt;
<br>Element has no title attribute or the title attribute is empty
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,1,HEADER,0,SECTION,2,DIV,0,DIV,0,DIV,2,DIV,2,DIV,2,FORM,0,INPUT`<br>
Selector:<br>
`#s`
</details>

---



### FYI: What is 'invisible screen reader only text'?

Screen reader only text is text that is read out-loud to a screen reader user, but not displayed on the screen. Most websites already have a class for this called `.element-invisible` or `.sr-only`. If this website does not, add this class to a stylesheet:

```css
.sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0;
}
```

Example usage:

```html
<a class="read-more" href="fancy-cats.html">
    Read more
+    <span class="sr-only"> about fancy cats</span>
  </i>
</a>
```





## Some links do not have a discernible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Link text (and alternate text for images, when used as links) that is discernible, unique, and focusable improves the navigation experience for screen reader users. [Learn more](https://dequeuniversity.com/rules/axe/2.2/link-name?application=lighthouse).




### Mobile Search `<a>` links and other nearby misc links have no text inside.

This is a common issue with mobile navigation and mobile search. Sometimes they are not fully hidden from assistive technology.  Pinpointing a visual location can be difficult. This is the general vicinity of the issue:

__Visual location:__

![ not descriptive](assets/heart-8.png)

__HTML Location__:

```html
<div class="hart_table full_width search_table">
    <div class="hart_table_cell middle mobile_hide">
        <a href="http://sanford.duke.edu" target="_blank" class="category_title extrasmall thick hart_gray">
            Sanford school of public policy <span class="emphasis no_text_transform">at</span> Duke University
            <p></p> <!--  .category_title small thick -->
        </a></div><a href="http://sanford.duke.edu" target="_blank" class="category_title extrasmall thick hart_gray">
        <!--  .hart_table_cell -->
    </a>
    <div class="hart_table_cell middle search_wrapper"><a href="http://sanford.duke.edu" target="_blank"
            class="category_title extrasmall thick hart_gray">
        </a><a href="javascript:void(0);" id="mobile_search_wrapper" class="mobile_show"
            data-target=".search_input"><span class="glyphicon glyphicon-search"></span></a>
        <form role="search" method="get" class="search-form" action="https://hart.sanford.duke.edu/">
            <input type="text" name="s" id="s" class="search_input mobile_hide font-raleway" placeholder="Search">
            <button type="submit" class="search_submit mobile_hide">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </form>
    </div> <!--  .hart_table_cell -->
</div>
```

Since the links are empty, they may not be read to a screen reader or it will announce it as an empty link.  As a result, they will not know what the link does or where it would take them.

#### Suggested solution:

It is hard to provide definitive advice on this because I don't know the constraints, but in simple terms, there are empty links in this area. Please try to clean them out or label them if they are necessary.

Consider adding invisible screen reader only text to describe its purpose, when it has a purpose.  If the empty link is extraneous consider removing it.


`+` add `-` remove `~` change

```html
<div class="hart_table full_width search_table">
    <div class="hart_table_cell middle mobile_hide">
        ...
    </div>
-    <a href="http://sanford.duke.edu" target="_blank" class="category_title extrasmall thick hart_gray">
-    </a>
    <div class="hart_table_cell middle search_wrapper">
-       <a href="http://sanford.duke.edu" target="_blank" class="category_title extrasmall thick hart_gray">       
-       </a>
        <a href="javascript:void(0);" id="mobile_search_wrapper" class="mobile_show" data-target=".search_input">
            <span class="glyphicon glyphicon-search">
+              <span class="sr-only">Mobile search</span>
            </span>
        </a>
        <form role="search" method="get" class="search-form" action="https://hart.sanford.duke.edu/">
            ...
        </form>
    </div> <!--  .hart_table_cell -->
</div>
```


<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty or not visible
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,1,HEADER,0,SECTION,2,DIV,0,DIV,0,DIV,2,DIV,2,DIV,0,A`<br>
Selector:<br>
`.search_wrapper > .extrasmall[hrefS="sanford.duke.edu"][target="_blank"]`
</details>

<br>

---



### This link contains an image and has no descriptive text or alt tag.

The missing `alt` text makes it unclear what links purpose and where it will take the user. 

__Visual location:__

![ not descriptive](assets/hart-sanford-duke-edu-recent_news_story-col-sm-4-nth-child-3-recent_news_image-.png)

#### HTML location:

```html
<a href="https://hart.sanford.duke.edu/story/hart-leadership-program-announces-2019-2020-fellows/"><img width="420" height="300" src="https://hart.sanford.duke.edu/wp-content/uploads/2019/04/Hart-Fallows-2019-Composite-420x300.png" class=" wp-post-image" alt="" scale="0"></a>
```

#### Suggested solution:

Add `alt` text.

Good alt text for this would be `alt` text could be "2020 Fellows Elizabeth Nowlin, Luiza Perez, and Amulya Vadapalli". 

Just adding their names indicates a lot to a screen reader user.  They will know there are three new fellows. The people in the photo are women. And that they might come from diverse backgrounds.

There are some scenarios where using `alt=""` is valid. But anytime an image is surrounded by a link, and has no text accompanying it, it is required to have `alt` text because its the only way to identify what the link does.

<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty or not visible
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,4,SECTION,0,DIV,0,DIV,0,DIV,4,ARTICLE,0,DIV,0,A`<br>
Selector:<br>
`.recent_news_story.col-sm-4:nth-child(3) > .recent_news_image > a`
</details>

<br>

---

### 'Read more' and 'Learn more' links

When a screen reader user hears the words 'Read more' they will not know where it will go or where it will take them. It will just read 'Read more, Read more, Read more'.

This is a very common problem.  This is something Duke receives complaints about and Duke gets in trouble for consistently. 

![too much read more](assets/heart-4.png)

![too much learn more](assets/heart-5.png)


#### Suggested solution:

Two options to resolve:

1. Make the text more descriptive to so the user knows what it does and where it will take them.

 OR

2. Add hidden screen reader only text.

Screen reader only text is text that is read out-loud to a screen reader user, but not displayed on the screen. This website already have a class for this called `.sr-only`. 

(CCS part below already exists, the site does not need to add this. Just showing it as example.)

```css
.sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0;
}
```

HTML needs added: 

```html
<a class="read-more" href="fancy-cats.html">
    Read more
+    <span class="sr-only"> about fancy cats</span>
  </i>
</a>
```

---


## Some image elements do not have `[alt]` attributes [WCAG 1.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#non-text-content)

Informative elements should aim for short, descriptive alternate text. Decorative elements can be ignored with an empty alt attribute. [Learn more](https://dequeuniversity.com/rules/axe/2.2/image-alt?application=lighthouse).



__Visual location:__

Image missing `alt` attribute:

![Image missing alt tag](assets/hart-sanford-duke-edu-sandford_log.png)


__HTML location:__

```html
<img class="sandford_logo" style="margin-bottom:15px;" src="https://hart.sanford.duke.edu/wp-content/themes/hart/images/duke_sanford_logo.png" scale="0">
```

#### Suggested solution:

Add an `alt` attribute with an accurate description to the image or add invisible screen reader text.

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>Element does not have an alt attribute
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty or not visible
<br>Element has no title attribute or the title attribute is empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,5,FOOTER,0,DIV,2,DIV,0,DIV,0,IMG`<br>
Selector:<br>
`.sandford_logo`
</details>

---







## Some background and foreground colors do not have a sufficient contrast ratio. [WCAG 1.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#contrast-minimum)

Low-contrast text is difficult or impossible for many users to read. [Learn more](https://dequeuniversity.com/rules/axe/2.2/color-contrast?application=lighthouse).



### The element Sanford School of Public Policy at Duke link has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-middle-mobile_hide-hart_table_cell-extrasmall-href-sanford-duke-edu-target-_blank.png)


__HTML location:__

```html
<a href="http://sanford.duke.edu" target="_blank" class="category_title extrasmall thick hart_gray">
```

#### Suggested solution:

  Element has insufficient color contrast of 3.5 (foreground color: #959faa, background color: #33485e, font size: 7.5pt, font weight: bold). Expected contrast ratio of 4.5:1
  
It needs a little more contrast to pass WCAG.  #959FAA would be a valid substitute. Or any other color that meets 4.5:1 ratio.

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,1,HEADER,0,SECTION,2,DIV,0,DIV,0,DIV,2,DIV,0,DIV,0,A`<br>
Selector:<br>
`.middle.mobile_hide.hart_table_cell > .extrasmall[hrefS="sanford.duke.edu"][target="_blank"]`
</details>

---




### The smaller hero text has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-hart_white-copy-clearfi.png)


__HTML location:__

```html
<p class="copy clearfix hart_white">
```

#### Suggested solution:

  Element has insufficient color contrast of 3.63 (foreground color: #ffffff, background color: #e35943, font size: 15.0pt, font weight: normal). Expected contrast ratio of 4.5:1

__Minimally invasive option:__

a. Add a subtle compound drop shadow:

```css
.hart_orange_bg .hart_white {
    text-shadow: 1px 1px 1px #b94836, -1px -1px 2px #b94836;
}
```

Currently:

![welcome](assets/hart-2.png)

With drop shadow:

![welcome](assets/hart-1.png)

You can verify color contrast of text on gradients or with drop shadows with the Color Contrast Analyzer Extension: <https://chrome.google.com/webstore/detail/color-contrast-analyzer/dagdlcijhfbmgkjokkjicnnfimlebcll?hl=en> 

__Other options:__

b. Increase font size to 16px.

c. Make the background-color a little darker [Find a color with higher contrast](http://contrast-finder.tanaguru.com).


This issue is very borderline.  It would be nice if it had higher contrast, however, WCAG 2.0 AA does give a little wiggle room on bold text if it is large. It is pretty big and bold already.


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,0,SECTION,2,DIV,0,DIV,0,DIV,1,DIV,2,DIV,1,P`<br>
Selector:<br>
`.hart_white.copy.clearfix`
</details>

---



### The element _"Read Story"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-col-md-pull-6-read_more-arrow-hart_whit.png)


__HTML location:__

```html
<a class="read_more arrow hart_white clearfix" href="https://hart.sanford.duke.edu/story/dear-students-alumni-faculty-and-friends/">Read Story</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 3.63 (foreground color: #ffffff, background color: #e35943, font size: 9.0pt, font weight: bold). Expected contrast ratio of 4.5:1
  
See previous issue. 


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,0,SECTION,2,DIV,0,DIV,0,DIV,1,DIV,2,DIV,2,A`<br>
Selector:<br>
`.col-md-pull-6 > .read_more.arrow.hart_white`
</details>

---



### The element _"First day of Fall 2019 classes"_ (x3) has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-col-lg-4-event-hart_table-nth-child-2-event_content-text-left-top-h3-post_title-hart_whit.png)


__HTML location:__

```html
<a href="https://hart.sanford.duke.edu/event/sol-information-session-2/" class="post_title hart_white">SOL Information Session #2</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 1.84 (foreground color: #ffffff, background color: #78cebe, font size: 12.0pt, font weight: bold). Expected contrast ratio of 4.5:1
  
The contrast here is really low. We don't have any wiggle room on this color combination.

Options: 

a. Add a compound drop shadow, similar to the earlier example.

```css
.hart_teal_bg .hart_white {
	text-shadow: 1px 1px 2px #538e83, -1px -1px 4px #538e83, 1px 0px 8px #538e83
}
.hart_teal_bg .hart_white:hover {
	text-shadow: none;
}
```


Before:

![after shadow contrast](assets/hart-4.png)

After:

![before shadow contrast](assets/hart-3.png)

You can verify color contrast of text on gradients or with drop shadows with the Color Contrast Analyzer Extension: <https://chrome.google.com/webstore/detail/color-contrast-analyzer/dagdlcijhfbmgkjokkjicnnfimlebcll?hl=en> 

b. Change the white to a dark color instead.

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,2,SECTION,2,DIV,0,DIV,2,DIV,5,ARTICLE,2,DIV,0,H3,0,A`<br>
Selector:<br>
`.col-lg-4.event.hart_table:nth-child(4) > .event_content.text-left.top > h3 > .post_title.hart_white`
</details>

---



### The element _"Read More"_ (x3) has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-recent_news_story-col-sm-4-nth-child-2-founders-cta-cta-orang.png)


__HTML location:__

```html
<a class="cta small orange full_width founders-cta" href="https://hart.sanford.duke.edu/story/hart-leadership-program-announces-inaugural-cohort-of-north-american-fellows/">Read More</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 3.63 (foreground color: #ffffff, background color: #e35943, font size: 10.5pt, font weight: bold). Expected contrast ratio of 4.5:1
  
See "__The smaller hero text has low contrast.__" for suggestions.


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,4,SECTION,0,DIV,0,DIV,0,DIV,2,ARTICLE,4,A`<br>
Selector:<br>
`.recent_news_story.col-sm-4:nth-child(2) > .founders-cta.cta.orange`
</details>

---


### The element _"ESTD 1986"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-hart_white-thick-category_titl.png)


__HTML location:__

```html
<p class="hart_white category_title thick">EST<sup>D</sup> 1986</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 1.84 (foreground color: #ffffff, background color: #78cebe, font size: 12.0pt, font weight: bold). Expected contrast ratio of 4.5:1
  
See "__The element _"First day of Fall 2019 classes"_ (x3) has low contrast.__" for suggestions.


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,6,SECTION,0,DIV,0,DIV,0,ARTICLE,0,P`<br>
Selector:<br>
`.hart_white.thick.category_title`
</details>

---


### The element _"Tell me More"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-read_more-arrow-hart_orang.png)


__HTML location:__

```html
<a class="read_more arrow hart_orange" href="/purpose/leadership-for-public-life/">Tell me More</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 1.96 (foreground color: #e35943, background color: #78cebe, font size: 9.0pt, font weight: bold). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,6,SECTION,0,DIV,0,DIV,0,ARTICLE,3,P,0,A`<br>
Selector:<br>
`.read_more.arrow.hart_orange`
</details>

---



### The element _"Get Involved"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-get_involved-container-row-article-nth-child-1-section_title-hart_blu.png)


__HTML location:__

```html
<span class="section_title hart_blue">Get Involved</span>
```

#### Suggested solution:

  Element has insufficient color contrast of 2.34 (foreground color: #33485e, background color: #178ba0, font size: 10.5pt, font weight: bold). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,8,SECTION,0,DIV,0,DIV,0,ARTICLE,1,SPAN`<br>
Selector:<br>
`#get_involved > .container > .row > article:nth-child(1) > .section_title.hart_blue`
</details>

---



### The element _"or"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-emphasized-text-cente.png)


__HTML location:__

```html
<p class="emphasized text-center">or</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 2.34 (foreground color: #33485e, background color: #178ba0, font size: 15.0pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,8,SECTION,0,DIV,0,DIV,0,ARTICLE,4,P`<br>
Selector:<br>
`.emphasized.text-center`
</details>

---



### The element _"Explore our core programs"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-hart_white-text-center-category_titl.png)


__HTML location:__

```html
<p class="category_title hart_white text-center">Explore our core programs</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 4 (foreground color: #ffffff, background color: #178ba0, font size: 12.0pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,8,SECTION,0,DIV,0,DIV,0,ARTICLE,5,P`<br>
Selector:<br>
`.hart_white.text-center.category_title`
</details>

---



### The element _"Learn More"_ (x3) has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-sol-wrapper-hart_white_bg-data-mh-program-cta_wrapper-cta-orange-full_widt.png)


__HTML location:__

```html
<a class="cta small orange full_width" href="https://hart.sanford.duke.edu/programs/sol/">Learn More</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 3.63 (foreground color: #ffffff, background color: #e35943, font size: 10.5pt, font weight: bold). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,8,SECTION,0,DIV,0,DIV,1,ARTICLE,0,DIV,3,DIV,0,A`<br>
Selector:<br>
`.sol > .wrapper.hart_white_bg[data-mh="program"] > .cta_wrapper > .cta.orange.full_width`
</details>

---




### The element _"Tony Brown"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-quote_compan.png)


__HTML location:__

```html
<p class="quote_company text-center hart_orange headline small">Tony Brown</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 1.74 (foreground color: #e35943, background color: #fb9f2f, font size: 45.0pt, font weight: bold). Expected contrast ratio of 3:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,10,SECTION,0,DIV,0,DIV,3,ARTICLE,2,P`<br>
Selector:<br>
`.quote_company`
</details>

---



### The element _"Read Story"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-read_more-arrow-orang.png)


__HTML location:__

```html
<a class="read_more  text-center arrow orange" href="https://hart.sanford.duke.edu/story/tony-brown/">Read Story</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 1.74 (foreground color: #e35943, background color: #fb9f2f, font size: 9.0pt, font weight: bold). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,3,MAIN,10,SECTION,0,DIV,0,DIV,3,ARTICLE,4,A`<br>
Selector:<br>
`.read_more.arrow.orange`
</details>

---



### The element _"All content ©2015 Hart Leadership Program. All Rights Reserved."_ has low contrast.

__Visual location:__

![Text with low contrast](assets/hart-sanford-duke-edu-copyrigh.png)


__HTML location:__

```html
<div class="copyright hart_gray">All content ©2015 Hart Leadership Program. All Rights Reserved.</div>
```

#### Suggested solution:

  Element has insufficient color contrast of 4.23 (foreground color: #959faa, background color: #3a3a3a, font size: 10.5pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,DIV,5,FOOTER,0,DIV,2,DIV,4,DIV,0,DIV`<br>
Selector:<br>
`.copyright`
</details>

---











## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

__Success!__

---




## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

Pay special attention to popup windows like ads or email signup solicitations. 

Currently, when a keyboard user or screen readers user tabs through the main menu the tab focus is lost in the sub menu. Not to worry. The fix on this website is super simple.

#### Suggested solution:

Automatically open sub menu items so the keyboard user has access to see the submenu.


Add this CSS:

```css
.main_nav .menu-item.faux-hover .sub-menu-wrapper {
    left: 0;
    opacity: 1;
    display: block;
    -webkit-transition-duration: 0s, .2s;
    -o-transition-duration: 0s, .2s;
    transition-duration: 0s, .2s;
    -webkit-transition-delay: 0s, 0s;
    -o-transition-delay: 0s, 0s;
    transition-delay: 0s, 0s;
}
```


Add this JS:

```js
$(document).ready(function () {

  $("#menu-header-menu > li > a")
    .hover(function (e) {
      $("#menu-header-menu > li").removeClass('faux-hover');
    })
    .keydown(function (e) {
      if ((e.keyCode === 9 && e.shiftKey)) { // tab + shift
        $(this).parent().prev().addClass('faux-hover');
      } else if (e.keyCode === 9) { // just tab
        $("#menu-header-menu > li").removeClass('faux-hover');
        $(this).parent().addClass('faux-hover');
      }
    })
    .focus(function (e) {
      $("#menu-header-menu > li").removeClass('faux-hover');
    });
});
```

__Success criteria:__

You will know it is successful if:

1. The sub menu automatically expands when the user tabs across it.
2. The sub menu automatically expands when the user tab+shift backwards across it.

__FYI__: There are two valid solutions for drop down menus.

1. Auto expand sub menu - I recommend this solution on this website because the menu does not have many menu items. It simplifies the experience for the screen reader user. And it simplifies maintaining the code, because we can avoid using complicated ARIA attributes. So it is a win-win on this specific scenario. This is ok for about 20 menu items or less.

2. Add down arrow keyboard accessibility with ARIA attributes that sync. - We have had people implement this solution on menus with 100 links. It is not acceptable for those scenarios, because it would take a screen reader user about 10 minutes to tab through the menu. Which would not be acceptable as an equally effective menu experience. 



---



## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

__Almost.__

Homepage Success after "The user's focus is directed to new content added to the page"

---





## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#interactive_elements_like_links_and_buttons_should_indicate_their_purpose_and_state).

Pay special attention to buttons and links.  For example, links and buttons should have obvious `:hover` and `:focus` states that meet WCAG 2.0 AA contrast requirements.

__Mixed results.__

In simple language, visitors need to be able to tell the difference between normal text on a page and clickable links without the need to hover over things to hunt for clickable items. 

This is especially important for touch screen's like phones and tablets. There is no hover state. So if people can't tell what is clickable the website becomes very hard to navigate.

Admittedly deciding what is good enough is a little subjective. Please read [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics) for more info. Please just be mindful of this rule and make any adjustments you see fit based on that criteria. Often this is accomplished with an underline that shows and hides on mouse hover. But having high contrast between normal link verses hover can achieve a valid result without an underline. It appears like 

One thing that is not subjective, is the need to have `:hover` states on all links.  It looks like the homepage has discernible hover states on it's links.

Some top level landing pages are missing hover states. See "Other top level landing pages". 


---




## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

__Success!__

---





## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

__Not applicable.__

---





## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

__Not applicable.__

---





## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

[Textise](https://www.textise.net/) is a neat tool for inspecting the natural order of the website. [View this website on Textise](https://www.textise.net/showText.aspx?strURL=hart.sanford.duke.edu/). If nothing has been done in JS to interfere the natural tab order, looking at that or viewing the source will basically follow the order of the markup.

__Success!__

---






## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Almost.__

Success after "The user's focus is directed to new content added to the page"

---





## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#take_advantage_of_headings_and_landmarks).

__Satisfactory.__

Headings skip levels from `<h1>` to `<h3>` sometimes. Since they are still roughly hierarchical this is not a significant barrier to people with disabilities. No action is needed.

---





## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#take_advantage_of_headings_and_landmarks).

Most websites are required to have some way to skip to the main content of a webpage. This can be done by using skip-to by-pass block as the first keyboard focusable link on the page.  Alternatively, or in addition to that method adding landmarks for `<header>`, `<footer>`, and `<main>` attribute will allow screen reader users to easily access all parts of the webpage.

__Success!__


---


### Missing Skip to link / bypass block missing. 

Adding ways to bypass repetitive content allows keyboard users navigate the page more efficiently. [Learn more](https://dequeuniversity.com/rules/axe/2.2/bypass).

The skip-to bypass block needs to be the first link on the page. When a user hits the tab key it gives it focus and as a result becomes visible. If the user hits the enter key while it is focused it will jump to the main content area.

#### Suggested solution:
Make the skip-to link bypass block jump to the main content area.


Keyboard and screen reader users expect the the skip-to link trigger to be the first focusable element in the `<body>` element.

```css
.element-invisible {
    margin: 0;
    padding: 0;
    width: 1px;
    position: absolute !important;
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    overflow: hidden;
    height: 1px;
}
.element-invisible.element-focusable:active, 
.element-invisible.element-focusable:focus {
    position: static !important;
    clip: auto;
    overflow: visible;
    height: auto;
}
  
```

This will keep the following HTML hidden, and only show it to keyboard only users.  It needs to be the first link on the page.

```html
<body>
+  <div id="skip-link">
+    <a href="#main" class="element-invisible element-focusable">Skip to main content</a>
+  </div>
...
```

__Success criteria__:

You will know it works if:

1. It is invisible when the page loads.

2. When you hit the tab key, it is the first element that receives focus on the page.

---


## Narrow viewport navigation functionality on desktop

Mobile nav is a new issue that Duke has had complaints about.  When people with low vision zoom in on a website, it usually triggers the mobile nav to show up.

The problem with the menu button is that it has a device dependent onclick event handler is present. That means these links work with a mouse, but not a keyboard. These links also cannot receive keyboard focus because they are just `<div>` or `<p>` elements. `<div>`s cand `<p>` elements cannot receive focus by default.

__Visual location:__

![device depended click handler on menu](assets/heart-6.png)


#### Suggested solution:

Make it an `<a>` anchor so it can receive focus.

OR

__Alternative solution:__

If there is a reason an anchor cannot be used here, the `<div>` would need a `tabindex="0"` attribute to make it focusable and it will need additional keyboard event listeners to detect the enter key when the element is focused.

Example:

```js
var $fauxDivLink = $('#nav_trigger');

$fauxDivLink.attr('tabindex', '0'); // if you can't add the tabindex to the div in real life, you can add it here.

 $fauxDivLink.keyup(function (e) {
  if (e.keyCode === 13 || e.keyCode === 32) { // key press for WCAG
    $fauxDivLink.click();
  }
});
```

---

## Other

It is fair to prioritize the homepage at the moment. The following issues are worth keeping in mind as next steps.

### Stories page

https://hart.sanford.duke.edu/stories/

Although this page is very cool, its interactive elements are not accessible to keyboard users, screen reader users, and deaf users.

- The links on the Quilt are not keyboard accessible. 
- The pop-up that deploys when the Quilt squares are clicked does not receive focus.
- Videos must be captioned. Its important for people with disabilities, and a time bomb legally.

Interactive elements and multimedia can be difficult to make accessible after development. The general rule is that if it cannot be made accessible, an equally effective accessible alternative needs to be available to everyone has access to the web content. At this point, it's not clear which option would be most cost effective.  Lets revisit it after the issues above are remediated.

### Other top-level landing pages

Links need to have discernible hover states.  The homepage have `:hover` states, but some links on subpages do not. Please take a look and try to add hover states.



<br>

<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.